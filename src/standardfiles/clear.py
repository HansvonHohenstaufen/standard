# See LICENSE file for copyright and license details.
"""Module to clean metadata."""
import filecmp
import re
import subprocess
from multiprocessing.dummy import Pool as ThreadPool

import magic

from standardfiles.message import message
from standardfiles.options import config
from standardfiles import utils


def clear_metadata():
	"""Clean the metadata from standard files."""

	message.process("Clear metadata")
	utils.create_tmpdir()
	list_files = utils.get_standard_files(
		config.options.mode.recursive
	)

	if list_files['img']:
		message.message("Images")
		pool = ThreadPool(8)
		pool.map(__remove_img, list_files['img'])
		pool.close()
		pool.join()
	if list_files['pdf']:
		message.message("Pdf")
		pool = ThreadPool(8)
		pool.map(__remove_pdf, list_files['pdf'])
		pool.close()
		pool.join()
	if list_files['audio']:
		message.message("Audio")
		for file in list_files['audio']:
			utils.check_size(file)
			__remove_audio(file)
	if list_files['video']:
		message.message("Video")
		for file in list_files['video']:
			utils.check_size(file)
			__remove_video(file)

	utils.remove_tmpdir()

	return 0


def __remove_img(img):
	"""Remove all metadata from img.

	The condition are.
		- Only accept png.
		- Do nothing with gif.
	"""

	mime = magic.Magic(mime=True)
	mime_type = mime.from_file(img)

	# Check if image is png
	if mime_type != "image/png":
		# If image is a gif, do nothing
		if mime_type != "image/gif":
			message.warning(f"It isn't png: {img}")
		return

	tmpfile = utils.get_tmpfile_name()
	command = f"png2ff < {img} | ff2png > {tmpfile}"
	try:
		subprocess.run(
			command,
			shell=True,
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			check=True
		)
	except subprocess.CalledProcessError:
		message.warning(f"Don't clear metadata from png: {img}")
		return

	__replace_file(img, tmpfile)


def __remove_pdf(pdf):
	"""Remove all metadata from pdf."""

	tmpfile = utils.get_tmpfile_name()
	tmpfile = f"{tmpfile}.pdf"
	command = f"exiftool -all= {pdf} -o {tmpfile}"
	status, _ = utils.run_command(command.split())
	if status:
		message.warning(f"Don't clear metadata from pdf: {pdf}")

	__replace_file(pdf, tmpfile)


def __remove_audio(audio):
	"""Remove all metadata from standard audio."""

	if not __check_audio_metadata(audio):
		return

	# Clear metadata
	message.file(audio)
	command = f"metaflac --remove {audio}"
	status, _ = utils.run_command(command.split())
	if status:
		message.warning(f"Don't clear metadata from audio: {audio}")
		return

	if __check_audio_metadata(audio):
		message.warning(f"Don't clear metadata from audio: {audio}")


def __remove_video(video):
	"""Remove all metadata from standard audio."""

	if not __check_video_metadata(video):
		return

	# Clear metadata
	tmpfile = utils.get_tmpfile_name()
	tmpfile = f"{tmpfile}.mkv"
	command = f"ffmpeg -i {video} -map_metadata -1 -c copy -y {tmpfile}"
	status, _ = utils.run_command(command.split())
	if status:
		message.warning(f"Don't clear metadata from video: {video}")
		return

	if __check_video_metadata(tmpfile):
		message.warning(f"The metadata don't remove from video: {video}")

	__replace_file(video, tmpfile)


def __check_audio_metadata(audio):
	"""Check metadata from standard audio."""

	command = "ffprobe -loglevel error -show_entries stream_tags:format_tags "
	command = f"{command} {audio}"

	status, info = utils.run_command(command.split())
	if status:
		message.warning(f"Don't check metadata from audio: {audio}")

	info = re.sub(".*(STREAM|FORMAT).*\n", "", info)
	if 0 < len(info):
		return -1
	return 0


def __check_video_metadata(video):
	"""Check metadata from standard video."""

	command = "ffprobe -loglevel error -show_entries stream_tags:format_tags "
	command = f"{command} {video}"
	status, info = utils.run_command(command.split())
	if status:
		message.warning(f"Don't check metadata from audio: {video}")
		return 0

	info = re.sub(".*(STREAM|FORMAT|TAG:DURATION=|TAG:ENCODER=).*\n", "", info)
	if 0 < len(info):
		return -1
	return 0


def __replace_file(file_original, tmpfile):
	"""Replace original file if it is different from tmpfile."""

	# If the same file
	if filecmp.cmp(file_original, tmpfile):
		return
	# Replace file
	message.file(file_original)
	command = f"cp {tmpfile} {file_original}"
	status, _ = utils.run_command(command.split())
	if status:
		message.warning(f"Don't replace: {file_original}")


__all__ = ['clear_metadata']
