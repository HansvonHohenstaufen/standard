# See LICENSE file for copyright and license details.
"""Module to convert images."""
import os
from multiprocessing.dummy import Pool as ThreadPool

from standardfiles.message import message
from standardfiles.utils import get_standard_img, run_command


def image_to_png():
	"""Convert from .ff.bz2 to .png."""

	message.process("Convert images from ff.bz2 to png")

	list_img = get_standard_img()
	if list_img:
		pool = ThreadPool(8)
		pool.map(__img2png, list_img)
		pool.close()
		pool.join()

	return 0


def __img2png(img):
	"""Thread to convert from .ff.bz2 to .png."""

	name = os.path.splitext(img)[0]
	name = os.path.splitext(name)[0]
	name = f"{name}.png"
	command = f"bunzip2 < {img} | ff2png > {name}"
	message.rename(img, name)
	status, _ = run_command(command, shell=True)
	if status:
		message.error(f"Don't convert to png: {img}")
		return -1
	return 0


__all__ = ['image_to_png']
