# See LICENSE file for copyright and license details.
"""Module to check the names."""
import os

from standardfiles.message import message
from standardfiles.utils import get_all


def check_name():
	"""Erase all spaces from files/directories names."""

	message.process("Check names")
	files = get_all()
	for file in files:
		old = str(file)
		new = old.replace(" ", "_")
		if old != new:
			os.rename(old, new)
			message.rename(old, new)
	return 0


__all__ = ['check_name']
