# See LICENSE file for copyright and license details.
"""Module to get args from command."""
import argparse
from dataclasses import fields

from standardfiles.options import config
from standardfiles.options.options import Options, OptionsMode


LIST_ARGS = {
	"options": {
		"--recursive": {
			"short": "-r",
			"help": "run the action recursively",
		},
		"--notmp": {
			"short": "-T",
			"help": "Don't use /tmp, run in current directory",
		},
	},
	"actions": {
		"--clear": {
			"short": "-c",
			"help": "Remove all metadata",
		},
		"--image": {
			"short": "-i",
			"help": "Convert images from ff.bz2 to png",
		},
		"--name": {
			"short": "-n",
			"help": "Remove all space in the file and directory name",
		},
		"--version": {
			"short": "-v",
			"help": "Print the version",
		},
	},
}


class CustomArgumentParser(argparse.ArgumentParser):
	"""Replace help."""

	def format_help(self):
		help_text = super().format_help()
		return help_text.replace("options:", "actions:")


def get_args():
	"""Get arguments."""

	message_usage = (
		"\n"
		" %(prog)s [options] [actions]\n"
	)
	message_description = (
		"Convert to standard format all images, videos and audios"
	)

	parser = CustomArgumentParser(
		usage=message_usage,
		description=message_description
	)

	for opt, args in LIST_ARGS['actions'].items():
		arg = [args.pop("short", None), opt]
		parser.add_argument(*arg, **args, action="store_true")

	current_options = parser.add_argument_group("optional arguments")
	for opt, args in LIST_ARGS['options'].items():
		arg = [args.pop("short", None), opt]
		current_options.add_argument(*arg, **args, action="store_true")

	return vars(parser.parse_args())


def set_config(args):
	"""Get the current option."""

	# Set actions
	options_elements = {field.name for field in fields(Options)}
	args_options = {
		k: v for k, v in args.items() if k in options_elements
	}
	config.options = Options(
		**{
			key: args_options.get(key, False)
			for key in Options.__annotations__
		}
	)

	# Set options
	options_elements = {field.name for field in fields(OptionsMode)}
	args_options = {
		k: v for k, v in args.items() if k in options_elements
	}
	config.options.mode = OptionsMode(
		**{
			key: args_options.get(key, False)
			for key in OptionsMode.__annotations__
		}
	)


__all__ = ['get_args', 'set_config']
