# See LICENSE file for copyright and license details.
"""Module to get configuration."""
from standardfiles.options.options import Options, Configuration


options = Options()
config = Configuration()
