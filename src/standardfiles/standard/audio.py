# See LICENSE file for copyright and license details.
"""Convert audio."""
import re
import os

from standardfiles.message import message
from standardfiles.options import config
from standardfiles.utils import run_command


def convert_to_standard_audio(audio):
	"""Convert audios to aac."""

	message.file(audio)
	audio_name = os.path.splitext(audio)[0]

	tmpfile = f'{config.config.tmp_dir}/audio/{audio_name}'
	check_status, ext = check_audio_standard(audio)
	if check_status:
		command = (
			f"ffmpeg -i {audio} -vn -c:a aac -q 10 "
			f"-fflags +bitexact -flags:v +bitexact -flags:a +bitexact -y "
			f"{tmpfile}{ext}"
		)
	else:
		command = f"cp {audio} {tmpfile}{ext}"
	status, _ = run_command(command.split())
	if status:
		message.warning(f"Don't convert to audio: {audio}")
		return -1

	return 0


def check_audio_standard(audio):
	"""Check if the audio is standard file."""

	ext = ".aac"
	command = f"ffprobe {audio} 2>&1"
	status, info = run_command(command, shell=True)
	if status:
		message.warning(f"Don't identify the audio: {audio}")
		return -1, ext

	# Check video
	check_audio = re.findall("^  Stream.*: Video:", info, re.MULTILINE)
	if len(check_audio) != 0:
		return -2, ext

	status_loss = check_audio_codec(info)
	status_lossless = check_audio_codec(info, lossless=True)

	if not status_lossless:
		ext = ".flac"

	if status_loss and status_lossless:
		return -3, ext

	return 0, ext


def check_audio_codec(info, lossless=False, mkv=False):
	"""Check audio codec is standard file.

	lossless: flac.
	loss: aac.
	"""

	audio_codec = "aac"
	if lossless:
		audio_codec = "flac"

	audio_container = audio_codec
	if mkv:
		audio_container = "matroska"

	# Check container
	check_container = re.findall(
		f"^Input.*{audio_container}.*from",
		info,
		re.MULTILINE
	)
	if len(check_container) == 0:
		return -1

	# Check codec
	check_audio = re.findall("^  Stream.*: Audio:", info, re.MULTILINE)
	check_format = re.findall(
		f"^  Stream.*: Audio: {audio_codec}",
		info,
		re.MULTILINE
	)

	if len(check_format) != len(check_audio):
		return -2
	return 0
