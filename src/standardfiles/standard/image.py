# See LICENSE file for copyright and license details.
"""Convert images."""
import os

from standardfiles.message import message
from standardfiles.options import config
from standardfiles.utils import get_tmpfile_name, run_command


def convert_to_standard_image(img):
	"""Convert images to ff.bz2.

	The result is save in tmp directory.

	Steps.
	1. Convert to png.
	2. Convert to ff and compress to ff.bz2
	"""

	command = f"identify {img}"
	status, info = run_command(command.split())
	if status:
		message.warning(f"Don't identify the image: {img}")
		return -1

	type_img = info.split()[1]
	tmpfile = get_tmpfile_name()
	img_name = os.path.splitext(img)[0]
	tmpfile_ffbz2 = f'{config.config.tmp_dir}/image/{img_name}.ff.bz2'
	tmpfile = f'{tmpfile}.png'

	# Convert to png
	match type_img:
		case "WEBP":
			command = f"dwebp {img} -o {tmpfile}"
		case "PNG":
			command = f"cp {img} {tmpfile}"
		case "GIF":
			return 0
		case _:
			command = f"convert {img} {tmpfile}"
	message.format(img, type_img)

	status, info = run_command(command.split())
	if status:
		message.warning(f"Don't convert to png: {img}")
		os.remove(tmpfile)
		return -2

	# Convert to ff.bz2
	command = f"png2ff < {tmpfile} | bzip2 > {tmpfile_ffbz2}"
	status, info = run_command(command, shell=True)
	if status:
		message.warning(f"Don't convert to ff.bz2: {img}")
		os.remove(tmpfile)
		os.remove(tmpfile_ffbz2)
		return -3

	os.remove(tmpfile)
	return 0
