# See LICENSE file for copyright and license details.
"""Convert videos."""
import re
import os

from standardfiles.message import message
from standardfiles.options import config
from standardfiles.utils import run_command
from standardfiles.standard.audio import check_audio_codec


def convert_to_standard_video(video):
	"""Convert video to mkv with h264 and acc."""

	message.file(video)
	video_name = os.path.splitext(video)[0]
	tmpfile = f'{config.config.tmp_dir}/video/{video_name}.mkv'

	if check_video_standard(video):
		command = (
			f"ffmpeg -i {video} -c:v libx264 -c:a aac "
			f"-fflags +bitexact -flags:v +bitexact -flags:a +bitexact -y "
			f"{tmpfile}"
		)
	else:
		command = f"cp {video} {tmpfile}"
	status, _ = run_command(command.split())
	if status:
		message.warning(f"Don't convert to video: {video}")
		return -1

	return 0


def check_video_standard(video):
	"""Check if the video is standard file."""

	command = f"ffprobe {video} 2>&1"
	status, info = run_command(command, shell=True)
	if status:
		message.warning(f"Don't identify the video: {video}")
		return -1

	# Check mkv
	check_mkv = re.findall("^Input.*matroska.*from", info, re.MULTILINE)
	if len(check_mkv) == 0:
		return -2

	# Check audio
	if check_all_codec(info):
		return -3
	return 0


def check_all_codec(info):
	"""Check video and audio codec."""

	status_audio_loss = check_audio_codec(info, mkv=True)
	status_audio_lossless = check_audio_codec(info, lossless=True, mkv=True)
	if status_audio_loss and status_audio_lossless:
		return -1

	status_video_loss = check_video_codec(info)
	status_video_lossless = check_video_codec(info, lossless=True)
	if status_video_loss and status_video_lossless:
		return -2

	if not (status_video_loss or status_audio_loss):
		return 0
	if not (status_video_lossless or status_audio_lossless):
		return 0

	return -3


def check_video_codec(info, lossless=False):
	"""Check video codec is standard file.

	lossless: ffv1.
	loss: h264.
	"""

	video_codec = "h264"
	if lossless:
		video_codec = "ffv1"

	# Check codec
	check_video = re.findall("^  Stream.*: Video:", info, re.MULTILINE)
	check_format = re.findall(
		f"^  Stream.*: Video: {video_codec}",
		info,
		re.MULTILINE
	)

	if len(check_format) != len(check_video):
		return -1
	return 0
