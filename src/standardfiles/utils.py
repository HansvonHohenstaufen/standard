# See LICENSE file for copyright and license details.
"""Module to utils functions."""
import os
import re
import subprocess
import sys
import uuid
from pathlib import Path

import magic

from standardfiles.message import message
from standardfiles.options import config


def check_size(files):
	"""Check file size."""

	if config.options.mode.notmp:
		return

	files_size = 0
	if isinstance(files, str):
		files_size = int(os.path.getsize(files) / 1024)
	else:
		for file in files:
			files_size = files_size + int(os.path.getsize(file) / 1024)

	# Get tmp size
	command = "df"

	status, info = run_command(command.split())
	if status:
		message.error("Don't get current /tmp size")
		sys.exit(-1)

	info = re.findall("^.* /tmp$", info, re.MULTILINE)
	info = info[0].split()
	free_tmp = int(info[3])
	proportion = files_size / free_tmp

	# Check free RAM
	if 0.8 < proportion:
		proportion = str(int(proportion * 100))
		message.warning(
			f"Don't enough free space in /tmp: {proportion}"
		)
		sys.exit(-1)


def create_tmpdir():
	"""Create tmp directory.

	The tmp directory can be the next options.
	- In RAM /tmp/standardfiles_XXXXX (default)
	- In the current directory, in this mode the command create a tmp
		directory.
	"""

	message.step("Create tmp directory")
	new_tmp = None
	if config.options.mode.notmp:
		current_tmp = os.getcwd()
		new_tmp = uuid.uuid4().hex[:8]
		new_tmp = f"{current_tmp}/tmp_{new_tmp}"
		# Remove directory
		command = f"rm -rf {new_tmp}"
		status, _ = run_command(command.split())
		if status:
			message.error("Can't delete the ./tmp directory")
			sys.exit(-1)
	else:
		new_tmp = uuid.uuid4().hex[:8]
		new_tmp = f"/tmp/standardfiles_{new_tmp}"

	# Create tmp
	try:
		os.makedirs(new_tmp)
	except OSError:
		message.error("Can't create tmp directory: {new_tmp}")
		sys.exit(-1)

	config.config.tmp_dir = new_tmp


def get_all():
	"""Return a list of directories and files in current directory."""

	return list(Path().glob('*'))


def get_files(recursive=False):
	"""Get in a dict all files in current directory by mime class."""

	# Get all files list
	if recursive:
		message.step("Search files (recursive)")
		all_file_list = Path().rglob('*')
	else:
		message.step("Search files")
		all_file_list = Path().glob('*')

	file_list = []
	for file in all_file_list:
		if os.path.isfile(file):
			file_list.append(str(file))

	return __class_mime(file_list)


def get_standard_files(recursive=False):
	"""Get in a dict all standard files in current directory by mime class."""

	# Get all files list
	if recursive:
		message.step("Search standard files (recursive)")
		all_file_list = Path().rglob('*')
	else:
		message.step("Search standard files")
		all_file_list = Path().glob('*')

	file_list = []
	for file in all_file_list:
		if os.path.isfile(file):
			file_list.append(str(file))

	return __class_mime_standard(file_list)


def get_standard_img():
	"""Get all standard images (.ff.bz2)."""

	img = []
	for file in Path().glob('*.ff.bz2'):
		if os.path.isfile(file):
			img.append(str(file))
	return img


def get_tmpfile_name():
	"""Get a tmp name from file in tmp directory."""

	new_file = uuid.uuid4().hex
	return f"{config.config.tmp_dir}/{new_file}"


def remove_tmpdir():
	"""Remove tmp directory."""

	message.step("Remove tmp directory")
	command = f"rm -rf {config.config.tmp_dir}"
	status, _ = run_command(command.split())
	if status:
		message.error("Can't delete the tmp directory: {tmp_dir}")
		sys.exit(-1)


def run_command(command, shell=False):
	"""Run a command"""

	try:
		info = subprocess.run(
			command,
			shell=shell,
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			check=True
		).stdout.decode('utf-8')
	except subprocess.CalledProcessError:
		return -1, None

	return 0, info


def __class_mime(file_list):
	"""Classify the file list.

	The category are.
	- image.
	- pdf.
	- audio.
	- video
	"""

	standard_files = {
		'img': [],
		'pdf': [],
		'audio': [],
		'video': [],
	}

	mime = magic.Magic(mime=True)
	for file in file_list:
		mime_type = mime.from_file(file)
		if mime_type.find("image") != -1:
			standard_files['img'].append(file)
			continue
		if mime_type.find("pdf") != -1:
			standard_files['pdf'].append(file)
			continue
		if mime_type.find("audio") != -1:
			standard_files['audio'].append(file)
			continue
		if mime_type.find("video") != -1:
			standard_files['video'].append(file)
			continue

	return standard_files


def __class_mime_standard(file_list):
	"""Classify the file list.

	The category are.
	- image.
	- pdf.
	- audio.
	- video
	"""

	standard_files = {
		'img': [],
		'pdf': [],
		'audio': [],
		'video': [],
	}

	mime = magic.Magic(mime=True)
	for file in file_list:
		match mime.from_file(file):
			case "image/png":
				standard_files['img'].append(file)
			case "application/pdf":
				standard_files['pdf'].append(file)
			case "audio/x-hx-aac-adts" | "audio/flac":
				standard_files['audio'].append(file)
			case "video/x-matroska":
				standard_files['video'].append(file)

	return standard_files


__all__ = [
	'check_size',
	'create_tmpdir',
	'get_all',
	'get_files',
	'get_standard_files',
	'get_standard_img',
	'get_tmpfile_name',
	'remove_tmpdir',
	'run_command',
]
